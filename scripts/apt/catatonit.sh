cd $CI_PROJECT_DIR
. scripts/header.sh
(
    set -x
    time apt-get update --quiet=2
    if ! time apt-get install \
            --no-install-suggests \
            --no-install-recommends \
            --quiet=2 \
        catatonit \
        mypy \
        python3 \
        python3-venv \
      > /tmp/apt 2>&1
    then cat /tmp/apt ; false
    fi

    python3 -m venv \
            --system-site-packages \
        .
)
set -x # Ubuntu debogging
. ./bin/activate

command -v script
apt-cache policy bsdutils

env --version
command -v sh
ls -l $(command -v sh)
apt-cache policy dash
exec catatonit -- env "$@"