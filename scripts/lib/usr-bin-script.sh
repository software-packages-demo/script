scriptf () {
    \script \
        --quiet \
        --return \
        --command "$(echo $@)"
}