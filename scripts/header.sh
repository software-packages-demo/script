set -o errexit -o nounset -o noglob
( set -o pipefail 2> /dev/null
) \
&& set -o pipefail \
|| true
command -v shopt > /dev/null && shopt -s failglob
(
    echo $CI_JOB_IMAGE
    . /etc/os-release
    echo $PRETTY_NAME ${VERSION_ID:-}
)

export TIME=$(printf '\n\e[90;47m')%e$(printf '\e[0m seconds')
export TIMEFORMAT=$(printf '\n\e[90;47m')%R$(printf '\e[0m seconds')

chmod u+x $CI_PROJECT_DIR/scripts/mypy_wrap.py
PATH=$PATH:$CI_PROJECT_DIR/scripts