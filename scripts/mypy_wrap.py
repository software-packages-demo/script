#! /usr/bin/python3
import subprocess
import sys

subp = [ 'script'
       , '--return'
       , '--quiet'
       , '--command'
       , 'mypy --pretty --color-output '
+' '.join(sys.argv[1:])]

# print(subp)

subprocess.run(subp, check=True)