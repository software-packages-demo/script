. $CI_PROJECT_DIR/scripts/header.sh
(
    set -x
    time apk add \
            --update-cache \
            --quiet \
        catatonit \
        python3 \
        py3-mypy \
        util-linux-misc \
        ;
# util-linux-misc for /usr/bin/script
)
exec catatonit -- env "$@"
