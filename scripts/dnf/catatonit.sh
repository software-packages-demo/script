. $CI_PROJECT_DIR/scripts/header.sh
(
    set -x
    time dnf install \
            --assumeyes \
            --quiet \
            --setopt=install_weak_deps=False \
        catatonit \
        python-unversioned-command \
        python-mypy \
        util-linux \
        ;
    # util-linux for /usr/bin/script
)
exec /usr/libexec/catatonit/catatonit -- env "$@"