<?php
# echo getcwd() . "\n";
# var_dump($argv);
# $c = implode(" ", array_slice($argv, 1));
# $c = $argv[1]." ".$argv[2]." '".$argv[3]."'";
$c = implode(" ", array_map(fn($s): string => "'".$s."'", array_slice($argv, 1)));
# $c = implode("\ ", array_map(fn($s): string => '"'.$s.'"', array_slice($argv, 1)));
# $c = implode("\ ", array_slice($argv, 1));
var_dump($c);
$d = <<<"EOD"
    (sleep 1 ; echo env $c ; echo ; sleep 1 ; cat /tmp/initial_input) \
    | script \
        --quiet \
        --return \
    EOD;
$d = <<<"EOD"
    (sleep 1 ; echo exec ps) \
    | script \
        --quiet \
        --return \
    EOD;
$d = <<<"EOD"
    echo exec ps ­­> /tmp/ps_input
    script \
        --quiet \
        --return \
        < /tmp/ps_input
    EOD;
# file_put_contents("/tmp/cli_input",
    # $c.PHP_EOL,
    # LOCK_EX);
# file_put_contents("/tmp/ps_input",
    # "exec ps".PHP_EOL,
    # FILE_APPEND | LOCK_EX);
# file_put_contents("/tmp/ps_input",
    # file_get_contents("/tmp/initial_input"),
    # FILE_APPEND | LOCK_EX);
file_put_contents(
    "/tmp/script_input",
    ". /tmp/initial_input".PHP_EOL
);
$d = <<<"EOD"
    exec script --quiet --return \
        --command "$c" \
        < /tmp/script_input
    EOD;
var_dump($d);
# $e = <<<"EOD"
    # sh -c
    # "script --quiet --return --command \"$c\" < /tmp/script_input"
    # EOD;
# var_dump($e);
# pcntl_exec("/usr/bin/env", array_slice($argv, 1), getenv());
# pcntl_exec("/bin/sh", ["-c", $argv[1]." ".$argv[2]." \"".$argv[3]."\""], getenv());

# pcntl_exec("/sbin/tini",
    # [ "--"
    # , $e
    # ], getenv());
pcntl_exec("/bin/sh",
    [ "-c"
    , $d
    ], getenv());
pcntl_exec("/bin/sh",
    [ "-c"
    , "script --quiet --return --command '$c' < /tmp/initial_input"
    ], getenv());
pcntl_exec("/usr/bin/script",
    [ "--quiet"
    , "--return"
    , "--command"
    , "sh -c 'cat /tmp/initial_input | $c'"
    ], getenv());
pcntl_exec("/usr/bin/php"
    , array_merge(["command.php"]
        , array_slice($argv, 1))
    , getenv());
pcntl_exec("/usr/bin/script",
    [ "--quiet"
    , "--return"
    , "--command"
    , "php command.php $c"
    ], getenv());
?>
