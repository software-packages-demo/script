# . $CI_PROJECT_DIR/scripts/header.sh
set -o errexit -o nounset -o noglob
( set -o pipefail 2> /dev/null
) \
&& set -o pipefail \
|| true
command -v shopt > /dev/null && shopt -s failglob

export TIME=$(printf '\n\e[90;47m')%e$(printf '\e[0m seconds')
export TIMEFORMAT=$(printf '\n\e[90;47m')%R$(printf '\e[0m seconds')

chmod u+x $CI_PROJECT_DIR/scripts/mypy_wrap.py
PATH=$PATH:$CI_PROJECT_DIR/scripts

(
case $(. /etc/os-release ; echo $ID) in
    alpine|wolfi)
        # set -x
        # time 
        apk add \
                --update-cache \
                --quiet \
            php-cli \
            php-pcntl \
            python3 \
            py3-mypy \
            tini \
            util-linux-misc \
            ;
# util-linux-misc for /usr/bin/script
        ;;
    debian|ubuntu)
        bash $CI_PROJECT_DIR/scripts/apt/entrypoint.sh
        #^ bash for builtin time command
        ;;
    fedora)
        case $CI_JOB_IMAGE in
            fedora \
            |quay.io/fedora/fedora)
                export DNF=dnf
                ;;
            quay.io/fedora/fedora-minimal)
                export DNF=microdnf
                ;;
            *)
                echo ERROR: Unknown image
                exit 2
                ;;
        esac
        set -x
        time $DNF install \
                --assumeyes \
                --quiet \
                --setopt=install_weak_deps=False \
            psmisc \
            tini \
            ;
        ;;
    *)
        echo ERROR: Unknown distribution
        exit 1
        ;;
esac
)

cat > /tmp/initial_input
chmod u+x /tmp/initial_input

# set -x
exec tini -- php $CI_PROJECT_DIR/scripts/entrypoint.php "$@"
exec php $CI_PROJECT_DIR/scripts/entrypoint.php "$@"
# c=$(echo env "$@")
# exec tini -- env "$@"
# exec tini -- $c
exec script \
    --quiet \
    --return \
    --command 'php $CI_PROJECT_DIR/scripts/entrypoint.php "$@"'

